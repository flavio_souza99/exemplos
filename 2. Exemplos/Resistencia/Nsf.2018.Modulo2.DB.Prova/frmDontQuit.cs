﻿using Nsf._2018.Modulo2.DB.Prova.DB.Resistencia;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.Modulo2.DB.Prova
{
    public partial class frmDontQuit : Form
    {
        public frmDontQuit()
        {
            InitializeComponent();
            txtNome.Select(0, 0);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ResistenciaDTO dto = new ResistenciaDTO();
            dto.Nome = txtNome.Text;
            dto.Mensagem = txtMensagem.Text;

            ResistenciaBusiness business = new ResistenciaBusiness();
            business.Salvar(dto);

            MessageBox.Show("A mensagem foi enviada com sucesso.");
        }

        private void btnListar_Click(object sender, EventArgs e)
        {
            ResistenciaBusiness business = new ResistenciaBusiness();
            List<ResistenciaDTO> lista = business.Listar();

            dgvMensagens.AutoGenerateColumns = false;
            dgvMensagens.DataSource = lista;
        }
    }
}
